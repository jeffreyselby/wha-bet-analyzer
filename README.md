# Author Details #

* Name: Jeffrey Selby
* Email: selby(at)venomousbyte.com
* Mobile: zero four six eight eight four nine one three one


### Description ###

* Application to upload bet data (both settled and unsettled).
* After file upload, analysis can be performed.


### Install Application ###

* THE BELOW Setup.exe DOES NOT WORK. GIT INTERFERES WITH THE DEPLOYED Manifest.exe.
* Download the repository "Wha Bet Analyzer"
* Double click on "Package\setup.exe"


### View Source Code ###

* Download the repository "Wha Bet Analyzer"
* Open the file "Wha.sln" in Visual Studio 2013.
* IMPORTANT: Ensure "Wha.Bet.UI" is the "Startup Project" and press F5 to run application from IDE.
* Projects in the solution are:
	Wha.Bet.Business 		(assembly which performs analysis of bet data)
	Wha.Bet.UI 				(WPF executable application)
	Wha.FileManager 		(assembly which uploads file data into DataTable)
* Test projects in the solution are:
	TestWha.Bet.Business 	(unit tests for Wha.Bet.Business)
	TestWha.Bet.FileManager (unit tests for Wha.Bet.FileManager)


### Instructions to Use Application ###

* Upon opening application, click on tab "File Loader" followed by "Load Data".
* In the main panel select "Settled" and upload file "Files\Settled.csv".
* Click on "Run" to upload settled bets.
* In the main panel select "Unsettled" and upload file "Files\Unsettled.csv".
* Click on "Run" to upload unsettled bets.

* Now click on "Data Analysis" followed by "Bet Analysis".
* Filter for bets (stake or win) with "Amount At or Above" input value.

* Now click on "Data Analysis" followed by "Customer Analysis".
* Filter for customers which have a "Win Ratio at or Above" a given percentage e.g. 50%.
* Filter for customers which have unsettled bets being a multiple above average stake for settled bets.