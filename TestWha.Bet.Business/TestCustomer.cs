﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wha.Bet.Business;
using System.Linq;
using System.Collections.Generic;

namespace TestWha.Bet.Business
{
    [TestClass]
    public class TestCustomer : TestBase
    {
        public TestCustomer()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        [ClassInitialize()]
        public static void TestCustomerInitialize(TestContext testContext)
        {
            // Create and set BetAnalyzer
            TestBase.TestBaseInitialize(testContext);

            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;

            IEnumerable<Customer> customers = betAnalyzer.Customers;

            Customer = betAnalyzer.Customers.FirstOrDefault(tempCustomer => tempCustomer.Id == 1);
        }

        [TestMethod]
        public void TestExpectedCashFlow()
        {
            decimal expectedCashFlow;

            expectedCashFlow = Math.Round((decimal)Customer.ExpectedCashFlow, 2);
            Assert.AreEqual(9110.00m, expectedCashFlow);
        }

        [TestMethod]
        public void TestWonAverage()
        {
            decimal wonAverage;

            wonAverage = Math.Round((decimal)Customer.WonAverage, 2);
            Assert.AreEqual(255.71m, wonAverage);
        }

         [TestMethod]
        public void TestWonBets()
        {
            int wonBetCount;

            wonBetCount = Customer.WonBets.Count();
            Assert.AreEqual(7, wonBetCount);
        }

        [TestMethod]
        public void TestWonRatio()
        {   
            decimal wonRatio;

            wonRatio = (decimal)Customer.WonRatio;
            Assert.AreEqual(0.70m, wonRatio);
        }

        [TestMethod]
        public void TestWonTotal()
        {            
            decimal wonTotal;

            wonTotal = Math.Round(Customer.WonTotal, 2);
            Assert.AreEqual(1790.00m, wonTotal);
        }

        private static Customer Customer
        {
            get;
            set;
        }
    }
}
