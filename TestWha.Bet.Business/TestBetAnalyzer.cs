﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wha.Bet.Business;
using System.Linq;

namespace TestWha.Bet.Business
{
    /// <summary>
    /// Summary description for TestBetAnalyzer
    /// </summary>
    [TestClass]
    public class TestBetAnalyzer : TestBase
    {
        public TestBetAnalyzer()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        [ClassInitialize()]
        public static void TestBetAnalyzerInitialize(TestContext testContext)
        {
            // Create and set BetAnalyzer
            TestBase.TestBaseInitialize(testContext);
        }

      
        [TestMethod]
        public void TestGetAverageStake()
        {
            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;
            decimal averageStake;



            averageStake = Math.Round((decimal)betAnalyzer.GetAverageStake(BetStatus.Settled), 2);
            Assert.AreEqual(58.00m, averageStake);

            averageStake = Math.Round((decimal)betAnalyzer.GetAverageStake(BetStatus.Unsettled), 2);
            Assert.AreEqual(177.50m, averageStake);

            averageStake = Math.Round((decimal)betAnalyzer.GetAverageStake(BetStatus.All), 2);
            Assert.AreEqual(94.51m, averageStake);
                        
        }

        [TestMethod]
        public void TestGetAverageWin()
        {
            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;
            decimal averageWin;

            averageWin = Math.Round((decimal)betAnalyzer.GetAverageWin(BetStatus.Settled), 2);
            Assert.AreEqual(181.70m, averageWin);

            averageWin = Math.Round((decimal)betAnalyzer.GetAverageWin(BetStatus.Unsettled), 2);
            Assert.AreEqual(1208.64m, averageWin);

            averageWin = Math.Round((decimal)betAnalyzer.GetAverageWin(BetStatus.All), 2);
            Assert.AreEqual(495.49m, averageWin);
        }

        [TestMethod]
        public void TestGetBetCount()
        {
            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;
            int betCount;

            betCount = betAnalyzer.GetBetCount(BetStatus.Settled);
            Assert.AreEqual(50, betCount);

            betCount = betAnalyzer.GetBetCount(BetStatus.Unsettled);
            Assert.AreEqual(22, betCount);

            betCount = betAnalyzer.GetBetCount(BetStatus.All);
            Assert.AreEqual(72, betCount);
        }

        [TestMethod]
        public void TestGetBetsStakeAbove()
        {
            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;
            int betCount;
            decimal threshold = 200;

            betCount = betAnalyzer.GetBetsStakeAbove(BetStatus.Settled, threshold).Count();
            Assert.AreEqual(2, betCount);
            
            betCount = betAnalyzer.GetBetsStakeAbove(BetStatus.Unsettled, threshold).Count();
            Assert.AreEqual(6, betCount);
            
            betCount = betAnalyzer.GetBetsStakeAbove(BetStatus.All, threshold).Count();
            Assert.AreEqual(8, betCount);
        }

        [TestMethod]
        public void TestGetBetsWinAbove()
        {
            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;
            int betCount;
            decimal threshold = 1000;

            betCount = betAnalyzer.GetBetsWinAbove(BetStatus.Settled, threshold).Count();
            Assert.AreEqual(2, betCount);

            betCount = betAnalyzer.GetBetsWinAbove(BetStatus.Unsettled, threshold).Count();
            Assert.AreEqual(4, betCount);

            betCount = betAnalyzer.GetBetsWinAbove(BetStatus.All, threshold).Count();
            Assert.AreEqual(6, betCount);
        }

        [TestMethod]
        public void TestGetTotalStakes()
        {
            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;
            decimal totalStakes;

            totalStakes = betAnalyzer.GetTotalStakes(BetStatus.Settled);
            Assert.AreEqual(2900m, totalStakes);

            totalStakes = betAnalyzer.GetTotalStakes(BetStatus.Unsettled);
            Assert.AreEqual(3905m, totalStakes);

            totalStakes = betAnalyzer.GetTotalStakes(BetStatus.All);
            Assert.AreEqual(6805m, totalStakes);
        }

        [TestMethod]
        public void TestGetTotalWins()
        {
            BetAnalyzer betAnalyzer = TestBase.BetAnalyzer;
            decimal totalWins;

            totalWins = betAnalyzer.GetTotalWins(BetStatus.Settled);
            Assert.AreEqual(9085, totalWins);

            totalWins = betAnalyzer.GetTotalWins(BetStatus.Unsettled);
            Assert.AreEqual(26590, totalWins);

            totalWins = betAnalyzer.GetTotalWins(BetStatus.All);
            Assert.AreEqual(35675, totalWins);
        }        
    }
}
