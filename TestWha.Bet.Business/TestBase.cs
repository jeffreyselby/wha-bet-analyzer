﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Wha.Bet.Business;
using System.Linq;

namespace TestWha.Bet.Business
{
    [TestClass]
    public class TestBase
    {     
        public TestBase()
        { }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [ClassInitialize()]
        public static void TestBaseInitialize(TestContext testContext)
        {
            SetBetAnalyzer();
        }


        private static DataTable GetBetsDataTable(bool isSettled)
        {
            DataTable dataTable = new DataTable();

            dataTable.Columns.Add("CustomerId", typeof(string));
            dataTable.Columns.Add("EventId", typeof(string));
            dataTable.Columns.Add("ParticipantId", typeof(string));
            dataTable.Columns.Add("Stake", typeof(string));
            dataTable.Columns.Add("Win", typeof(string));

            if (isSettled)
            {
                // Settled (50 rows)
                dataTable.Rows.Add("1", "1", "6", "50", "250");
                dataTable.Rows.Add("2", "1", "3", "5", "0");
                dataTable.Rows.Add("3", "1", "3", "20", "0");
                dataTable.Rows.Add("4", "1", "6", "200", "1000");
                dataTable.Rows.Add("1", "2", "1", "20", "60");
                dataTable.Rows.Add("2", "2", "1", "5", "15");
                dataTable.Rows.Add("3", "2", "2", "50", "0");
                dataTable.Rows.Add("4", "2", "5", "100", "0");
                dataTable.Rows.Add("5", "2", "3", "20", "0");
                dataTable.Rows.Add("1", "3", "5", "50", "0");
                dataTable.Rows.Add("2", "3", "5", "10", "0");
                dataTable.Rows.Add("3", "3", "5", "20", "0");
                dataTable.Rows.Add("4", "3", "5", "100", "0");
                dataTable.Rows.Add("5", "3", "6", "20", "0");
                dataTable.Rows.Add("6", "3", "1", "50", "500");
                dataTable.Rows.Add("1", "4", "1", "50", "120");
                dataTable.Rows.Add("2", "4", "5", "10", "0");
                dataTable.Rows.Add("3", "4", "6", "50", "0");
                dataTable.Rows.Add("1", "5", "10", "50", "400");
                dataTable.Rows.Add("2", "5", "7", "10", "0");
                dataTable.Rows.Add("3", "5", "9", "20", "0");
                dataTable.Rows.Add("4", "5", "9", "200", "0");
                dataTable.Rows.Add("4", "5", "10", "250", "2000");
                dataTable.Rows.Add("5", "5", "10", "20", "160");
                dataTable.Rows.Add("6", "5", "10", "50", "400");
                dataTable.Rows.Add("1", "5", "7", "40", "0");
                dataTable.Rows.Add("1", "6", "4", "40", "160");
                dataTable.Rows.Add("2", "6", "3", "15", "0");
                dataTable.Rows.Add("3", "6", "4", "50", "0");
                dataTable.Rows.Add("4", "6", "5", "100", "0");
                dataTable.Rows.Add("4", "6", "4", "120", "480");
                dataTable.Rows.Add("5", "6", "4", "10", "40");
                dataTable.Rows.Add("2", "7", "8", "10", "0");
                dataTable.Rows.Add("2", "7", "7", "20", "0");
                dataTable.Rows.Add("3", "7", "3", "50", "300");
                dataTable.Rows.Add("1", "8", "5", "80", "320");
                dataTable.Rows.Add("5", "8", "8", "20", "0");
                dataTable.Rows.Add("1", "9", "2", "60", "480");
                dataTable.Rows.Add("1", "9", "4", "50", "0");
                dataTable.Rows.Add("2", "9", "3", "20", "0");
                dataTable.Rows.Add("2", "9", "10", "20", "0");
                dataTable.Rows.Add("3", "9", "10", "20", "0");
                dataTable.Rows.Add("3", "9", "2", "50", "400");
                dataTable.Rows.Add("4", "9", "2", "200", "1600");
                dataTable.Rows.Add("4", "9", "6", "300", "0");
                dataTable.Rows.Add("5", "9", "8", "30", "0");
                dataTable.Rows.Add("5", "9", "7", "25", "0");
                dataTable.Rows.Add("6", "9", "2", "50", "400");
                dataTable.Rows.Add("6", "9", "7", "40", "0");
                dataTable.Rows.Add("6", "9", "6", "50", "0");
            }
            else
            {
                // Unsettled Bets (22 rows)
                dataTable.Rows.Add("1", "11", "4", "50", "500");
                dataTable.Rows.Add("3", "11", "6", "50", "400");
                dataTable.Rows.Add("4", "11", "7", "300", "1200");
                dataTable.Rows.Add("5", "11", "2", "20", "80");
                dataTable.Rows.Add("1", "12", "4", "500", "5000");
                dataTable.Rows.Add("2", "12", "4", "20", "200");
                dataTable.Rows.Add("3", "12", "1", "50", "400");
                dataTable.Rows.Add("4", "12", "1", "250", "1000");
                dataTable.Rows.Add("5", "12", "5", "100", "800");
                dataTable.Rows.Add("6", "12", "3", "50", "400");
                dataTable.Rows.Add("6", "12", "2", "50", "200");
                dataTable.Rows.Add("1", "13", "3", "50", "200");
                dataTable.Rows.Add("2", "13", "1", "10", "50");
                dataTable.Rows.Add("3", "13", "9", "40", "400");
                dataTable.Rows.Add("4", "13", "6", "200", "1000");
                dataTable.Rows.Add("6", "13", "4", "500", "4000");
                dataTable.Rows.Add("1", "14", "2", "1000", "8000");
                dataTable.Rows.Add("2", "14", "5", "15", "60");
                dataTable.Rows.Add("3", "14", "8", "300", "900");
                dataTable.Rows.Add("4", "14", "5", "200", "800");
                dataTable.Rows.Add("5", "14", "6", "100", "600");
                dataTable.Rows.Add("6", "14", "6", "50", "400");
            }
            return dataTable;
        }

        private static void SetBetAnalyzer()
        {
            BetAnalyzer betAnalyzer = new BetAnalyzer();

            // Load settled bets
            betAnalyzer.LoadData(GetBetsDataTable(true), "CustomerId", "EventId", "ParticipantId", "Stake", "Win", true);

            // Load unsettled bets
            betAnalyzer.LoadData(GetBetsDataTable(false), "CustomerId", "EventId", "ParticipantId", "Stake", "Win", false);

            TestBase.BetAnalyzer = betAnalyzer;
        }
        
        public static BetAnalyzer BetAnalyzer
        {
            get;
            private set;
        }
    }
}
