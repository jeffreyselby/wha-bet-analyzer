﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wha.FileManager;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace TestWha.FileManager
{
    [TestClass]
    public class TestTextFileReader
    {
        private const string FilePath = @"..\..\..\Files\Settled.csv";


        [TestMethod]
        public void TestExecute()
        {

            TextFileReader textFileReader = new TextFileReader();

            textFileReader.FilePath = FilePath;
            

            Dictionary<string, string> mappings = new Dictionary<string,string>() {
                    {"Customer", "CustomerId"},                    
                    {"Event", "EventId"},
                    {"Participant", "ParticipantId"},
                    {"Stake", "Stake"},
                    {"Win", "Win"}};

            textFileReader.SetFieldToColumnMappings(mappings, 0);

            DataTable dataTable = textFileReader.Execute();

            Assert.IsTrue(dataTable.Rows.Count > 0);

            foreach (string columnName in mappings.Values)
            {
                Assert.IsTrue(dataTable.Rows[0][columnName].ToString().Length > 0);
            }           
        }
    }
}
