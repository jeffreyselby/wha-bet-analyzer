﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.FileManager
{
    public abstract class FileReader
    {
        public abstract DataTable Execute();
        
        public string FilePath
        {
            get;
            set;
        }        
    }
}
