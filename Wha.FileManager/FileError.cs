﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.FileManager
{
    public struct FileError
    {
        public int LineIndex
        {
            get;
            set;
        }

        public int LineText
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }
    }
}
