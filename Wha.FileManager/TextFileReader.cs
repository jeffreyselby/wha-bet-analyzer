﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Wha.FileManager
{
    public class TextFileReader : FileReader
    {
        private Dictionary<string, string> FieldNameToColumnNameMappings = null;
        private Dictionary<int, string> FieldIndexToColumnNameMappings = null;
        private IEnumerable<string> columnNames = null;
        private string delimiter = ",";
     
        public string Delimiter
        {
            get
            {
                return this.delimiter;
            }
            set
            {
                this.delimiter = value;
            }
        }

        private int HeaderRowIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Header rows to ignore in text file (default is 0).
        /// </summary>
        public int HeaderRowsToIgnore
        {
            get;
            set;
        }

        /// <summary>
        /// Footer rows to ignore in text file (default is 0).
        /// </summary>
        public int FooterRowsToIgnore
        {
            get;
            set;
        }

        /// <summary>
        /// Text File Start of Data Tag (rows below will be loaded)
        /// </summary>
        public string StartOfDataTag
        {
            get;
            set;
        }

        /// <summary>
        /// Text File End of Data Tag (rows above will be loaded)
        /// </summary>
        public string EndOfDataTag
        {
            get;
            set;
        }

        public void SetFieldToColumnMappings(Dictionary<string, string> fieldNameToColumnNameMappings, int headerRowIndex)
        {
            this.FieldIndexToColumnNameMappings = null;
            this.FieldNameToColumnNameMappings = fieldNameToColumnNameMappings;
            this.HeaderRowIndex = headerRowIndex;
        }

        public void SetFieldToColumnMappings(Dictionary<int, string> fieldIndexToColumnNameMappings)
        {
            this.FieldNameToColumnNameMappings = null;
            this.FieldIndexToColumnNameMappings = fieldIndexToColumnNameMappings;
            this.HeaderRowIndex = -1;
        }
        
        public override DataTable Execute()
        {            
            int lineIndex = 0;
            string lineText;
            string[] lineItems;            
            bool searchForStartOfDataTag = !String.IsNullOrEmpty(this.StartOfDataTag);
            bool searchForEndOfDataTag = !String.IsNullOrEmpty(this.EndOfDataTag);
            List<int> tempFieldIndexes = new List<int>();
            IEnumerable<string> fieldNames;
            IEnumerable<string> columnNames = this.ColumnNames;
            Dictionary<int, string> fieldIndexToColumnNameMappings = null;
            int lastLineIndex;

            DataTable dataTable = CreateDataTable();

            if (this.HeaderRowIndex == -1)
                // File content is read based on column index
                fieldIndexToColumnNameMappings = this.FieldIndexToColumnNameMappings;                
            else
                // File content is read based on column name.
                fieldIndexToColumnNameMappings = new Dictionary<int,string>();

            if (this.FooterRowsToIgnore > 0)
                lastLineIndex = File.ReadLines(base.FilePath).Count() - this.FooterRowsToIgnore - 1;
            else
                lastLineIndex = Int32.MaxValue;

            using (System.IO.StreamReader file = new System.IO.StreamReader(base.FilePath))
            {
                while ((lineText = file.ReadLine()) != null)
                {
                    if (searchForStartOfDataTag && lineText.StartsWith(this.StartOfDataTag))
                    {
                        // Tag for content start has been found.
                        // Start reading contents from next line.
                        searchForStartOfDataTag = false;
                    }
                    else
                    {
                        if (searchForEndOfDataTag && !searchForStartOfDataTag && lineText.StartsWith(this.EndOfDataTag))
                        {
                            // Tag for content end has been found.
                            // Stop reading file.
                            searchForEndOfDataTag = false;
                            break;
                        }
                        else
                        {
                            if (lineIndex >= this.HeaderRowsToIgnore)
                            {
                                if (this.HeaderRowIndex == lineIndex)
                                {
                                    // Field names are based on the header row index e.g. line 5 has headings for data.
                                    lineItems = lineText.Split(new string[] { this.Delimiter }, StringSplitOptions.None);

                                    fieldNames = this.FieldNameToColumnNameMappings.Keys;

                                    foreach (KeyValuePair<string, string> fieldNameToColumnNameMapping in this.FieldNameToColumnNameMappings)
                                    {
                                        // Create a Dictionary of Field Index to Column Name mappings.
                                        // Field Index is based on position of field name in header row.
                                        int index = Array.IndexOf(lineItems, fieldNameToColumnNameMapping.Key);

                                        if (index == -1)
                                        {
                                            // Field name does not exist in header row.
                                            // File cannot be correctly read.                                            
                                            throw new IOException(String.Format("Field name {0} cannot be found."));
                                        }
                                        else
                                        {
                                            // The field index in file will be mapped to column name of DataTable.
                                            fieldIndexToColumnNameMappings.Add(index, fieldNameToColumnNameMapping.Value);
                                        }
                                    }

                                    this.FieldIndexes = tempFieldIndexes;
                                    
                                }
                                else if (this.HeaderRowIndex < lineIndex && lineIndex <= lastLineIndex)
                                {
                                    lineItems = lineText.Split(new string[] { this.Delimiter }, StringSplitOptions.None);
                                    DataRow dataRow = dataTable.NewRow();
                                    
                                    foreach (KeyValuePair<int, string> fieldIndexToColumnNameMapping in fieldIndexToColumnNameMappings)
                                        dataRow[fieldIndexToColumnNameMapping.Value] = lineItems[fieldIndexToColumnNameMapping.Key];
                                    
                                    dataTable.Rows.Add(dataRow);
                                }
                            }
                        }                        
                    }

                    lineIndex++;
                }

                file.Close();
            }

            return dataTable;
        }

        private IEnumerable<int> FieldIndexes
        {
            get;
            set;
        }

        private IEnumerable<string> ColumnNames
        {
            get
            {
                if (this.columnNames == null)
                {
                    if (this.FieldIndexToColumnNameMappings != null)
                        this.columnNames = this.FieldIndexToColumnNameMappings.Values;
                    else if (this.FieldNameToColumnNameMappings != null)
                        this.columnNames = this.FieldNameToColumnNameMappings.Values;            
                }

                return this.columnNames;
            }
        }

        private DataTable CreateDataTable()
        {           
            DataTable dataTable = null;
            IEnumerable<string> tempColumnNames = this.ColumnNames;
                        
            if (tempColumnNames != null)
            {
                dataTable = new DataTable();

                foreach (string columnName in tempColumnNames)
                    dataTable.Columns.Add(columnName, typeof(string));                                    
            }

            return dataTable;
        }
    }
}
