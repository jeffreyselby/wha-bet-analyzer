﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.Bet.Business
{
    public abstract class BetCalculator
    {
        /// <summary>
        /// Fields storing outcome of calculations.
        /// </summary>
        private Dictionary<BetStatus, int> betCounts = new Dictionary<BetStatus, int>();        
        private Dictionary<BetStatus, decimal?> averageStakes = new Dictionary<BetStatus, decimal?>();
        private Dictionary<BetStatus, decimal?> averageWins = new Dictionary<BetStatus, decimal?>();
        private Dictionary<BetStatus, decimal> totalStakes = new Dictionary<BetStatus, decimal>();
        private Dictionary<BetStatus, decimal> totalWins = new Dictionary<BetStatus, decimal>();
        private Dictionary<BetStatus, IEnumerable<Bet>> filteredBets = new Dictionary<BetStatus, IEnumerable<Bet>>();
        private IEnumerable<Bet> wonBets = null;
        private IEnumerable<Bet> lostBets = null;
        private decimal? wonRatio = null;
        private decimal? winTotal = null;
        private decimal? wonAverage = null;
        private decimal? maximumAmountWon = null;
        private decimal? maximumAmountLost = null;
        private decimal? totalLost = null;
        
        public abstract ICollection<Bet> Bets
        {
            get;
        }
        
        /// <summary>
        /// Count bets for given status.
        /// </summary>
        /// <param name="betStatus">Settled, Unsettled, All</param>
        /// <returns></returns>
        public int GetBetCount(BetStatus betStatus)
        {
            if (!this.betCounts.ContainsKey(betStatus))
                this.betCounts.Add(betStatus, GetBets(betStatus).Count());

            return this.betCounts[betStatus];
        }

        
        /// <summary>
        /// Get average stake of bets for given status.
        /// </summary>
        /// <param name="betStatus">Settled, Unsettled, All</param>
        /// <returns></returns>
        public decimal? GetAverageStake(BetStatus betStatus)
        {
            if (!this.averageStakes.ContainsKey(betStatus))
            {
                IEnumerable<Bet> bets = GetBets(betStatus);

                // Note: Max on empty enumeration throws an exception
                if (bets.Count() == 0)
                    this.averageStakes.Add(betStatus, null);
                else
                    this.averageStakes.Add(betStatus, bets.Average(tempBet => tempBet.Stake));
            }

            return this.averageStakes[betStatus];
        }

        /// <summary>
        /// Get average win of bets for given status.
        /// </summary>
        /// <param name="betStatus">Settled, Unsettled, All</param>
        /// <returns></returns>
        public decimal? GetAverageWin(BetStatus betStatus)
        {
            if (!this.averageWins.ContainsKey(betStatus))
            {
                IEnumerable<Bet> bets = GetBets(betStatus);

                // Note: Max on empty enumeration throws an exception
                if (bets.Count() == 0)
                    this.averageWins.Add(betStatus, null);
                else
                    this.averageWins.Add(betStatus, GetBets(betStatus).Average(tempBet => tempBet.Win));
            }

            return this.averageWins[betStatus];
        }

        /// <summary>
        /// Total stakes for given bet status.
        /// </summary>
        /// <param name="betStatus">Settled, Unsettled, All</param>
        /// <returns></returns>
        public decimal GetTotalStakes(BetStatus betStatus)
        {
            if (!this.totalStakes.ContainsKey(betStatus))
                // Note: Sum on empty enumeration does not throw an exception
                this.totalStakes.Add(betStatus, GetBets(betStatus).Sum(tempBet => tempBet.Stake));

            return this.totalStakes[betStatus];
        }

        public decimal GetTotalWins(BetStatus betStatus)
        {
            if (!this.totalWins.ContainsKey(betStatus))
                // Note: Sum on empty enumeration does not throw an exception
                this.totalWins.Add(betStatus, GetBets(betStatus).Sum(tempBet => tempBet.Win));

            return this.totalWins[betStatus];
        }


        /// <summary>
        /// Total losses from customer's perspective
        /// </summary>
        public decimal TotalWon
        {
            get
            {
                return this.GetTotalWins(BetStatus.Settled);
            }
        }

        /// <summary>
        /// Total losses from customer's perspective
        /// </summary>
        public decimal TotalLost
        {
            get
            {
                if (this.totalLost == null)
                    // Note: Sum on empty enumeration does not throw an exception
                    this.totalLost = this.LostBets.Sum(tempBet => tempBet.Stake);
                
                return (decimal)this.totalLost;
            }            
        }


        /// <summary>
        /// Get all bets with stake above given threshold.
        /// </summary>
        /// <param name="betStatus"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public IEnumerable<Bet> GetBetsStakeAbove(BetStatus betStatus, decimal threshold)
        {
            return this.GetBets(betStatus).Where(tempBet => tempBet.Stake > threshold);

        }

        /// <summary>
        /// Get all bets with win above given threshold.
        /// </summary>
        /// <param name="betStatus"></param>
        /// <param name="threshold"></param>
        /// <returns></returns>
        public IEnumerable<Bet> GetBetsWinAbove(BetStatus betStatus, decimal threshold)
        {
            return this.GetBets(betStatus).Where(tempBet => tempBet.Win > threshold);
        }

        /// <summary>
        /// Total amount won.
        /// </summary>
        public decimal WonTotal
        {
            get
            {
                if (this.winTotal == null)
                    this.winTotal = this.WonBets.Sum(tempBet => tempBet.Win);

                return (decimal)this.winTotal;
            }
        }

        /// <summary>
        /// Return ratio of won bets over settled bets.
        /// Return null if there are not settled bets.
        /// </summary>
        public decimal? WonRatio
        {
            get
            {
                if (this.wonRatio == null)
                {
                    int settledBetsCount = GetBets(BetStatus.Settled).Count();

                    if (settledBetsCount > 0)
                        this.wonRatio = (decimal)this.WonBets.Count() / (decimal)settledBetsCount;
                }

                return this.wonRatio;
            }
        }

        /// <summary>
        /// Average win for those bets settled and won.
        /// </summary>
        public decimal? WonAverage
        {
            get
            {
                if (this.wonAverage == null)
                {
                    int wonBetCount = this.WonBets.Count();

                    if (wonBetCount > 0)
                        this.wonAverage = this.WonTotal / wonBetCount;
                }

                return (decimal)this.wonAverage;
            }
        }
        
        /// <summary>
        /// All won bets.
        /// </summary>
        public IEnumerable<Bet> WonBets
        {
            get
            {
                if (this.wonBets == null)
                    this.wonBets = this.Bets.Where(tempBet => tempBet.IsSettled && tempBet.Win > 0);

                return this.wonBets;
            }
        }

        public IEnumerable<Bet> LostBets
        {
            get
            {
                if (this.lostBets == null)
                    this.lostBets = this.Bets.Where(tempBet => tempBet.IsSettled && tempBet.Win == 0);

                return this.lostBets;
            }
        }
       
        public decimal? MaximumAmountWon
        {
            get
            {
                if (this.maximumAmountWon == null)
                {
                    if (this.WonBets.Count() > 0)
                        maximumAmountWon = this.WonBets.Max(tempBet => tempBet.Win);
                }
    
                return this.maximumAmountWon;
            }
        }

        public decimal? MaximumAmountLost
        {
            get
            {
                if (this.maximumAmountLost == null)
                {
                    if (this.LostBets.Count() > 0)
                        maximumAmountLost = this.LostBets.Max(tempBet => tempBet.Stake);                        
                }

                return this.maximumAmountLost;
            }
        }


        /// <summary>
        /// Get bets based on status (Settled, Unsettled, All)
        /// </summary>
        /// <param name="betStatus">Settled, Unsettled, All</param>
        /// <returns></returns>
        public IEnumerable<Bet> GetBets(BetStatus betStatus)
        {
            if (this.Bets == null)
            {
                this.filteredBets.Add(betStatus, Enumerable.Empty<Bet>());
            }
            else
            {
                if (!this.filteredBets.ContainsKey(betStatus))
                {
                    switch (betStatus)
                    {
                        case BetStatus.All:
                            this.filteredBets.Add(BetStatus.All, this.Bets);
                            break;

                        case BetStatus.Settled:
                            this.filteredBets.Add(betStatus, this.Bets.Where(tempBet => tempBet.IsSettled));
                            break;

                        case BetStatus.Unsettled:
                            this.filteredBets.Add(BetStatus.Unsettled, this.Bets.Where(tempBet => !tempBet.IsSettled));
                            break;
                    }
                }
            }

            return this.filteredBets[betStatus];
        }

        /// <summary>
        /// If a bet is added, then all calculations must be refreshed.
        /// </summary>
        public virtual void Refresh()
        {
            // Every time a new bet is added, the caculated values must be reset.
            this.betCounts.Clear();
            this.totalStakes.Clear();
            this.averageStakes.Clear();
            this.averageWins.Clear();
            this.totalWins.Clear();
            this.filteredBets.Clear();   
            
            this.wonBets = null;
            this.wonRatio = null;
            this.winTotal = null;
            this.wonAverage = null;
        }

        /// <summary>
        /// Clear all bets.
        /// </summary>
        public virtual void Clear()
        {
            // Reset calculations
            this.Refresh();

            // Clear all bets
            this.Bets.Clear();
        }        
    }
}
