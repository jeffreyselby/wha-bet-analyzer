﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.Bet.Business
{
    public class BetAnalyzer : BetCalculator
    {
        private ObservableCollection<Bet> bets = new ObservableCollection<Bet>();
        private IEnumerable<Customer> customers = null;
        
        public BetAnalyzer()
        {
            bets.CollectionChanged += Bets_CollectionChanged;            
        }

        private void Bets_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (Bet bet in e.NewItems)
                    if (bet.Customer != null)
                        bet.Customer.Bets.Add(bet);
            }   
            else if (e.OldItems != null)
            {
                foreach (Bet bet in e.OldItems)
                    if (bet.Customer != null)
                        bet.Customer.Bets.Remove(bet);
            }
            
            Debug.WriteLine("Bets_CollectionChanged");

            this.Refresh();
        }

        public void LoadData(DataTable dataTable, string customerIdColumnName, string eventIdColumnName, string participantIdColumnName, string stakeColumnName, string winColumnName, bool isSettled)
        {           
            Bet bet;
            int previousCustomerId = Int32.MinValue;
            int customerId;
            bool initialLoad = this.Bets.Count == 0 ? true : false;
            DataRow[] dataRows;
            
            BetAnalyzer betAnalyzer = new BetAnalyzer();
            Customer customer = null;

            DataView dataView = dataTable.DefaultView;

            string sortExpression = String.Format("{0} ASC", customerIdColumnName);

            if (initialLoad)
                // First load of bets
                dataRows = dataTable.Select(String.Empty, sortExpression);
            else
            {                
                // Subsequent load of bets. 
                // Get Customer for each bet.
                dataRows = dataTable.Select();
                //customers = this.Customers;
            }

            Debug.WriteLine("InitialLoad: " + initialLoad);

            foreach (DataRow dataRow in dataRows)
            {
                bet = new Bet();

                bet.EventId = Convert.ToInt32(dataRow[eventIdColumnName].ToString());
                bet.ParticipantId = Convert.ToInt32(dataRow[participantIdColumnName].ToString());
                bet.Stake = Convert.ToDecimal(dataRow[stakeColumnName].ToString());
                bet.Win = Convert.ToDecimal(dataRow[winColumnName].ToString());
                bet.IsSettled = isSettled;

                customerId = Convert.ToInt32(dataRow[customerIdColumnName].ToString());

                if (initialLoad)
                {
                    if (customerId != previousCustomerId)
                        customer = new Customer(customerId);
                } 
                else
                {
                    // Subsequent load of bets.
                    // Find customer if already exists in current bets, otherwise create new customer.
                    customer = this.Customers.FirstOrDefault(tempCustomer => tempCustomer.Id == customerId);

                    if (customer == null)
                        customer = new Customer(customerId);
                }


                bet.Customer = customer;

                this.Bets.Add(bet);
                Debug.WriteLine("Added new bet");

                previousCustomerId = customerId;
            }            
        }

        public override ICollection<Bet> Bets
        {
            get
            {
                return this.bets;
            }            
        }

        public IEnumerable<Customer> Customers
        {
            get
            {
                if (this.customers == null)
                    this.customers = this.Bets.Select<Bet, Customer>(tempBet => tempBet.Customer).Distinct();
                
                return this.customers;              
            }
        }
        
        public override void Refresh()
        {
            base.Refresh();

            this.customers = null;
        }       
    }
}
