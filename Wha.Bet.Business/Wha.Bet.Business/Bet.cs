﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.Bet.Business
{
    public class Bet
    { 
        public Customer Customer
        {
            get;
            set;
        }

        public int EventId
        {
            get;
            set;
        }

        public int ParticipantId
        {
            get;
            set;
        }

        public decimal Stake
        {
            get;
            set;
        }

        public decimal Win
        {
            get;
            set;
        }

        public bool IsSettled
        {
            get;
            set;
        }
    }
}
