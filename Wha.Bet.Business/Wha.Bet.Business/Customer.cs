﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.Bet.Business
{ 
    public class Customer : BetCalculator
    {                
        ObservableCollection<Bet> bets = new ObservableCollection<Bet>();
        private decimal? expectedCashFlow = null;

        public Customer()
        {
            bets.CollectionChanged += Bets_CollectionChanged;
        }

        public Customer(int id) : this()
        {
            this.Id = id;            
        }

        private void Bets_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null || e.OldItems != null)
            {
                this.Refresh();  // Required to refresh calculations             
            }         
        }

        public int Id
        {
            get;
            set;
        }

        public override ICollection<Bet> Bets
        {
            get
            {
                return this.bets;
            }
        }

        public decimal ActualCashFlow
        {
            get
            {
                return this.TotalWon - this.TotalLost;
            }
        }

        /// <summary>
        /// Consists of:
        ///     1. Plus: Loss ratio times stakes of unsettled bets.
        ///     2. Minus: Win ratio times potential win of unsettled bets.
        /// </summary>
        public decimal? ExpectedCashFlow
        {
            get
            {
                if (this.expectedCashFlow == null)
                {   
                    decimal? wonRatio = base.WonRatio;

                    if (wonRatio != null)
                        this.expectedCashFlow = wonRatio * base.GetTotalWins(BetStatus.Unsettled) - (1 - wonRatio) * base.GetTotalStakes(BetStatus.Unsettled);
                }

                return this.expectedCashFlow;
            }
        }

        
    }
}
