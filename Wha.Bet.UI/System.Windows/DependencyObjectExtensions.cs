﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Collections.ObjectModel;

namespace System.Windows
{
    /// <summary>
    /// Associated with WPF
    /// </summary>
    public static class DependencyObjectExtensions
    {
        public static bool HasChild(this DependencyObject parentDependencyObject, DependencyObject childDependencyObject, bool isRecursive)
        {
            return GetChildren(parentDependencyObject, true).Contains(childDependencyObject);
        }

        public static IEnumerable<DependencyObject> GetChildren(this DependencyObject dependencyObject, bool isRecursive)
        {
            Collection<DependencyObject> children = new Collection<DependencyObject>();

            FillChildren(children, dependencyObject, isRecursive);

            return children;                 
        }
               
        public static IEnumerable<T> GetChildren<T>(this DependencyObject dependencyObject, bool isRecursive) where T : DependencyObject
        {
            T child;
            Collection<DependencyObject> childDependencyObjects = new Collection<DependencyObject>();
            Collection<T> typedChildren = new Collection<T>();

            FillChildren(childDependencyObjects, dependencyObject, isRecursive);

            return childDependencyObjects.OfType<T>();        
        }


        private static void FillChildren(Collection<DependencyObject> allChildren, DependencyObject parentDependencyObject, bool isRecursive)
        {
            DependencyObject childObject;
            int childrenCount = VisualTreeHelper.GetChildrenCount(parentDependencyObject);

            for (int i = 0; i < childrenCount; i++)
            {
                childObject = VisualTreeHelper.GetChild(parentDependencyObject, i);

                allChildren.Add(childObject);

                if (isRecursive && VisualTreeHelper.GetChildrenCount(childObject) > 0)
                    FillChildren(allChildren, childObject, isRecursive);
            }
        }        
    }
}
