﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Controls
{
    public static class TabItemExtensions
    {
        public static bool HasContent(this TabItem tabItem, DependencyObject childObject, bool isRecursive)
        {
            bool hasChildObject = false;
            DependencyObject content = null;
            
            if (tabItem.Content == childObject)
                hasChildObject = true;
            else if (isRecursive)
            {
                content = tabItem.Content as DependencyObject;

                if (content != null)
                    hasChildObject = content.HasChild(childObject, true);                                
            }

            return hasChildObject;
        }
    }
}
