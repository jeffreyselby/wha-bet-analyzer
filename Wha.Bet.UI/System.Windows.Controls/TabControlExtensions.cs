﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Controls
{
    public static class TabControlExtensions
    {
        public static TabItem GetTabItem(this TabControl tabControl, DependencyObject childObject, bool isRecursive)
        {
            TabItem tabItem = null;

            foreach (TabItem tempTabItem in tabControl.Items)
            {
                if (tempTabItem.HasContent(childObject, isRecursive))
                {
                    tabItem = tempTabItem;
                    break;
                }
            }

            return tabItem;
        }
    }
}
