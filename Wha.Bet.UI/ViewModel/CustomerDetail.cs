﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wha.Bet.Business;

namespace Wha.Bet.UI.ViewModel
{
    public class CustomerDetail
    {
        private Customer customer = null;
            
        public CustomerDetail(Customer customer)
        {
            this.customer = customer;
        }

        public int Id
        {
            get
            {
                return this.customer.Id;
            }
        }

        public decimal? AverageStakeSettled
        {
            get
            {                
                return customer.GetAverageStake(BetStatus.Settled);               
            }            
        }

        public decimal? AverageWinSettled
        {
            get
            {                
                return customer.GetAverageWin(BetStatus.Settled);             
            }    
        }

        public decimal? AverageStakeUnsettled
        {
            get
            {
                return customer.GetAverageStake(BetStatus.Unsettled);
            }
        }

        public decimal? AverageWinUnsettled
        {
            get
            {
                return customer.GetAverageWin(BetStatus.Unsettled);
            }
        }

        public decimal? AverageStakeAll
        {
            get
            {
                return customer.GetAverageStake(BetStatus.All);
            }
        }

        public decimal? AverageWinAll
        {
            get
            {
                return customer.GetAverageWin(BetStatus.All);
            }
        }

      
        public int BetCountSettled
        {
            get
            {
                return customer.GetBetCount(BetStatus.Settled);
            }            
        }

        public int BetCountUnsettled
        {
            get
            {
                return customer.GetBetCount(BetStatus.Unsettled);
            }

        }
        
        public int BetCountAll
        {
            get
            {
                return customer.GetBetCount(BetStatus.All);
            }
        }

        public decimal? MaximumAmountWon
        {
            get
            {
                return customer.MaximumAmountWon;
            }            
        }

        public decimal? MaximumAmountLost
        {
            get
            {
                return customer.MaximumAmountLost;
            }
        }

        public decimal TotalWon
        {
            get
            {
                return customer.TotalWon;
            }
        }

        public decimal TotalLost
        {
            get
            {
                return customer.TotalLost;
            }
            
        }

        public decimal ActualCashFlow
        {
            get
            {
                return customer.ActualCashFlow;
            }
            
        }

        public decimal? ExpectedCashFlow
        {
            get
            {
                return customer.ExpectedCashFlow;
            }            
        }

        public IEnumerable<Business.Bet> Bets
        {
            get
            {
                return this.customer.Bets;
            }

        }
    }
}
