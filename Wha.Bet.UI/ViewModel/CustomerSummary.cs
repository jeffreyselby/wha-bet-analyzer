﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wha.Bet.Business;

namespace Wha.Bet.UI.ViewModel
{
    public class CustomerSummary
    {
        private Customer customer = null;
        private decimal multipleAboveAverageStake;
        private int? countHighUnsettledStakes = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="multipleAboveAverageStake">Expressed as a percentage above average stake for settled bets.
        /// Example: 20% means all stakes for unsettled bets which are 20% or higher compared to average stake for settled bets.</param>
        public CustomerSummary(Customer customer, decimal multipleAboveAverageStake)
        { 
            this.customer = customer;
            this.multipleAboveAverageStake = multipleAboveAverageStake;
        }

        public Customer Customer
        {
            get
            {
                return this.customer;
            }
        }

        public int CustomerId
        {
            get
            {
                return this.customer.Id;
            }
        }

        public decimal? WonRatio
        {
            get
            { 
                return customer.WonRatio;                
            }
        }

        public decimal ActualCashFlow
        {
            get
            {
                return customer.ActualCashFlow;
            }
        }

        public decimal? ExpectedCashFlow
        {
            get
            {  
                return customer.ExpectedCashFlow;                               
            }
        }

        public int? CountHighUnsettledStakes
        {
            get
            {
                if (countHighUnsettledStakes == null)
                {
                    decimal? averageStake = customer.GetAverageStake(BetStatus.Settled);

                    if (averageStake != null)
                        this.countHighUnsettledStakes = customer.GetBets(BetStatus.Unsettled).Count(tempBet => tempBet.Stake >= averageStake * (multipleAboveAverageStake));
                                        
                }

                return this.countHighUnsettledStakes;
            }
        }
    }
}
