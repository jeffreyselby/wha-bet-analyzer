﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wha.Bet.Business;
using Business = Wha.Bet.Business;

namespace Wha.Bet.UI.ViewModel
{
    public class BetDetail
    {
        private Business.Bet bet = null;
        private decimal? percentageToAverageStakeSettled = null;
        private decimal? percentageToAverageWinSettled = null;

        public BetDetail(Business.Bet bet)
        {
            this.bet = bet;
        }

        public bool IsSettled
        {
            get
            {
                return bet.IsSettled;
            }            
        }

        public decimal Stake
        {
            get
            {
                return bet.Stake;
            }
        }

        public decimal Win
        {
            get
            {
                return bet.Win;
            }
        }

        public decimal? PercentageToAverageStakeSettled
        {
            get
            {
                if (this.percentageToAverageStakeSettled == null)
                {
                    decimal? averageStakeSettled = this.bet.Customer.GetAverageStake(BetStatus.Settled);

                    if (averageStakeSettled != null)
                        this.percentageToAverageStakeSettled = bet.Stake / (decimal)averageStakeSettled;                    
                }

                return this.percentageToAverageStakeSettled;
            }
        }

        public decimal? PercentageToAverageWinSettled
        {
            get
            {
                if (this.percentageToAverageWinSettled == null)
                {
                    decimal? averageWinSettled = this.bet.Customer.GetAverageWin(BetStatus.Settled);

                    if (averageWinSettled != null)
                        this.percentageToAverageWinSettled = bet.Win / (decimal)averageWinSettled;
                }

                return this.percentageToAverageWinSettled;
            }
        }
     
    }
}
