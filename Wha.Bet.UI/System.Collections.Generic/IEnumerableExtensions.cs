﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Globalization;

namespace System.Collections.Generic
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Title: ToCollection<T>() Details
        /// Source: http://www.extensionmethod.net/Details.aspx?ID=238
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static Collection<T> ToCollection<T>(this IEnumerable<T> enumerable)
        {
            var collection = new Collection<T>();
            foreach (T i in enumerable)
                collection.Add(i);
            return collection;
        }

        /// <summary>
        /// ArrayList implements IList and ICollection
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static ArrayList ToArrayList(this IEnumerable enumerable)
        {
            ArrayList arrayList = new ArrayList();       // Implements IList and ICollection

            foreach (object item in enumerable)
                arrayList.Add(item);

            return arrayList;
        }

        #region Public Static Methods - To DataTable

        public static DataTable AsDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dataTable = new DataTable();
            var firstItem = collection.FirstOrDefault();
            if (firstItem != null)
            {
                PropertyInfo[] propertyInfos = firstItem.GetType().GetProperties();
                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    dataTable.Columns.Add(propertyInfo.Name, propertyInfo.PropertyType);
                }
                foreach (T item in collection)
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow.BeginEdit();
                    foreach (PropertyInfo propertyInfo in propertyInfos)
                    {
                        dataRow[propertyInfo.Name] = propertyInfo.GetValue(item, null);
                    }
                    dataRow.EndEdit();
                    dataTable.Rows.Add(dataRow);
                }
            }
            return dataTable;
        }
        /// <summary>
        /// Title: How to convert an IEnumerable to a DataTable in the same way as we use ToList or ToArray
        /// Source: http://blogs.msdn.com/dataaccesstechnologies/archive/2009/04/08/how-to-convert-an-ienumerable-to-a-datatable-in-the-same-way-as-we-use-tolist-or-toarray.aspx
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection, string tableName) // where T : System.Object
        {
            DataTable dataTable = ToDataTable(collection);
            dataTable.TableName = tableName;
            return dataTable;
        }

        /// <summary>
        /// Title: How to convert an IEnumerable to a DataTable in the same way as we use ToList or ToArray
        /// Source: http://blogs.msdn.com/dataaccesstechnologies/archive/2009/04/08/how-to-convert-an-ienumerable-to-a-datatable-in-the-same-way-as-we-use-tolist-or-toarray.aspx
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection) // where T : System.Object (not allowed)
        {
            DataTable dataTable = new DataTable();
            Type type = typeof(T);
            PropertyInfo[] propertyInfos = type.GetProperties();
            DataRow dataRow;

            if (propertyInfos.Length == 0)
                throw new ArgumentException("Generic argument is of wrong type");

            //Create the columns in the DataTable 
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
                    dataTable.Columns.Add(propertyInfo.Name, propertyInfo.PropertyType.GetGenericArguments()[0]);
                else
                    dataTable.Columns.Add(propertyInfo.Name, propertyInfo.PropertyType);
            }

            //Populate the table 
            foreach (T item in collection)
            {
                dataRow = dataTable.NewRow();
                dataRow.BeginEdit();

                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    if (propertyInfo.GetValue(item, null) == null)
                        dataRow[propertyInfo.Name] = DBNull.Value;
                    else
                        dataRow[propertyInfo.Name] = propertyInfo.GetValue(item, null);
                }

                dataRow.EndEdit();
                dataTable.Rows.Add(dataRow);
            }

            return dataTable;
        }

    
        #endregion

        public static IEnumerable<string> ToString<T>(this IEnumerable<T> enumerable)
        {
            Collection<string> items = new Collection<string>();
            foreach (T element in enumerable)
            {
                items.Add(element.ToString());
            }
            return items;
        }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
                action(item);
        }
    }
}
