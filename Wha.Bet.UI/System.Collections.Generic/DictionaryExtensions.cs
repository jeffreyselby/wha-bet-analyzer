﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace System.Collections.Generic
{
	public static class DictionaryExtensions
	{
        public static void Enforce<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            if (dictionary.ContainsKey(key))
                dictionary[key] = value;
            else
                dictionary.Add(key, value);
        }

		/// <summary>
		/// Extension method to add the collection of KeyValuePairs to another dictionary
		/// </summary>
		/// <typeparam name="TKey">Key of the dictionary entry</typeparam>
		/// <typeparam name="TValue">Value of the dictionary entry</typeparam>
		/// <param name="dictionary">Dictionary where the entries are added</param>
		/// <param name="collection">Enumeration of dictionary entries being added</param>
		public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<KeyValuePair<TKey, TValue>> collection)
		{
			foreach (KeyValuePair<TKey, TValue> keyValuePair in collection)
			{
				//dictionary.Add(keyValuePair);
				dictionary.Add(new KeyValuePair<TKey, TValue>(keyValuePair.Key, keyValuePair.Value));
			}
		}

		public static void AddRange<TKey, TValue, TItem>(this IDictionary<TKey, TValue> dictionary, IEnumerable<TItem> collection, Func<TItem, TKey> keyMethod, Func<TItem, TValue> valueMethod)
		{
			foreach (TItem item in collection)
			{
				TKey key = keyMethod(item);
				TValue value = valueMethod(item);

				//dictionary.Add(keyValuePair);
				dictionary.Add(new KeyValuePair<TKey, TValue>(key, value));
			}
		}

		/// <summary>
		/// XML String would be of this format:
		///  <DocumentElement>
		///      <MyTable><id>abc</id><username>UE0039</username><firstname>Jeff</firstname><lastname>Selby</lastname></MyTable>
		///  </DocumentElement>
		///  
		/// In the example above "DocumentElement" refers to rootNodeLocalName and "MyTable" refers to childNodeLocalName.
		/// Note: "MyTable" node only appears once. Inside this node the dictionary keys (xmlTag) and values (xmlValue) are listed.
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="dictionary"></param>
		/// <returns></returns>
		public static string ToXml<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string rootNodeLocalName, string childNodeLocalName)
		{
			string xmlString = String.Empty;

			if (dictionary.Count > 0)
			{
				XmlWriterSettings xmlWriterSettings;
				
				// Use an XMLWriter to write to memory stream.
				xmlWriterSettings = new XmlWriterSettings();
				xmlWriterSettings.Indent = false;
				xmlWriterSettings.Encoding = Encoding.ASCII;
				xmlWriterSettings.OmitXmlDeclaration = true;

				MemoryStream memoryStream = new MemoryStream();
				XmlWriter xmlWriter = XmlWriter.Create(memoryStream, xmlWriterSettings);		// Write Declaration
				xmlWriter.WriteStartDocument();
				string localName;
				string value;

				// Write the root node
				xmlWriter.WriteStartElement(rootNodeLocalName);		// e.g. "DocumentElement"

				// Write first (and only) record node
				xmlWriter.WriteStartElement(childNodeLocalName);					// e.g. "MyTable"

				foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
				{
					localName = keyValuePair.Key.ToString();
					value = keyValuePair.Value.ToString();
					xmlWriter.WriteElementString(localName, value);
				}
				
				xmlWriter.WriteEndElement();		// Close "Information" tag
				xmlWriter.WriteEndElement();		// Close "DocumentElement" tag

				// Close the document
				xmlWriter.WriteEndDocument();

				// Flush the write
				xmlWriter.Flush();

				Byte[] buffer = new Byte[memoryStream.Length];
				buffer = memoryStream.ToArray();
				xmlString = System.Text.Encoding.ASCII.GetString(buffer);
			}

			return xmlString;	// Has indentation
		}

	}
}
