﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Globalization;
using System.Collections.ObjectModel;
using IO = System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.ComponentModel;
using Wha.Bet.UI.Windows;
using Wha.Bet.UI.Settings;
using System.Configuration;
using System.IO;
using Wha.Bet.Business;
using Business = Wha.Bet.Business;
using Wha.Bet.UI.ViewModel;

namespace Wha.Bet.UI.UserControls
{
    /// <summary>
    /// Interaction logic for DatabaseParameterUserControl.xaml
    /// </summary>
    public partial class CustomerAnalysisUserControl : UserControl, IUserSettings
    {
        #region Members

        private CustomerDetailWindow customerDetailWindow = null;

        #endregion

        #region Constructors

        public CustomerAnalysisUserControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// Called by MainWindow
        /// </summary>
        void IUserSettings.SaveUserSettings()
        {
            
        }

        #endregion

        #region Private Methods - General

        private void LoadUserSettings()
        {
          
        }

        private bool ValidateForm()
        {
            bool isValid = true;
            decimal winRatio = 0;
            decimal unsettledStakesThreshold = 0;
            
            string message;

            // Win ratio
            string winRatioText = winRatioTextBox.Text.Trim();

            if (isValid && (bool)winRatioCheckBox.IsChecked)
            {
                if (!Decimal.TryParse(winRatioText, out winRatio))
                {
                    isValid = false;
                }
                else
                {
                    if (winRatio < 0 || winRatio > 100)
                        isValid = false;
                }
            }

            if (!isValid)
            {
                message = "Win Ratio must be a number between 0 and 100.";
                MessageBox.Show(message, "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                winRatioTextBox.Focus();
            }
            
            // Unsettled stakes threshold
            string unsettledStakesThresholdText = multipleAboveAverageStakeTextBox.Text.Trim();

            if (isValid && (bool)multipleAboveAverageStakeCheckBox.IsChecked)
            {
                if (!Decimal.TryParse(unsettledStakesThresholdText, out unsettledStakesThreshold))
                {
                    isValid = false;
                }
                else
                {
                    if (unsettledStakesThreshold < 1)
                        isValid = false;
                }
            }

            if (!isValid)
            {
                message = "Unsettled Stakes Threshold must be a number greater than or equal to 1.";
                MessageBox.Show(message, "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                multipleAboveAverageStakeTextBox.Focus();
            }

            return isValid;                
        }       
     
        #endregion

        #region Private Methods - Button Events

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateForm())
            {                
                BetAnalyzer betAnalyzer = Wha.Bet.UI.Properties.Settings.Default.BetAnalyzer;

                SetResultControls(betAnalyzer);
                             
                OutputVisibility = System.Windows.Visibility.Visible;                
            }
        }
        
        #endregion       
        
        #region Private Methods - UserControl Events

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadUserSettings();

            OutputVisibility = System.Windows.Visibility.Hidden;
        }

        #endregion

        /// <summary>
        /// Results
        /// </summary>
        private Visibility OutputVisibility
        {
            set
            {
                resultsGroupBox.Visibility = value;             
            }
        }

        protected void ResultsDataGridRow_MouseDoubleClick(object sender, EventArgs args)
        {
            var row = sender as DataGridRow;
            if (row != null && row.IsSelected)
            {
                CustomerSummary customerSummary = (CustomerSummary)row.Item;

                ShowCustomerDetailWindow(customerSummary);               
            }
        }

        private void ViewHyperlink_Click(object sender, RoutedEventArgs e)
        {
            CustomerSummary cusomterSummary = ((Hyperlink)e.OriginalSource).DataContext as CustomerSummary;

            if (cusomterSummary != null)
                ShowCustomerDetailWindow(cusomterSummary);          
        }

        private CustomerDetailWindow CustomerDetailWindow
        {
            get
            {
                if (this.customerDetailWindow == null)
                    this.customerDetailWindow = new CustomerDetailWindow();

                return this.customerDetailWindow;
            }
        }
      
        private void ShowCustomerDetailWindow(CustomerSummary customerSummary)
        {
                        
            this.CustomerDetailWindow.CustomerDetail = new CustomerDetail(customerSummary.Customer);

            if (this.CustomerDetailWindow.IsLoaded)
            {
                this.CustomerDetailWindow.Visibility = Visibility.Visible;               
            }
            else
            {
                this.CustomerDetailWindow.Show();
            }

            this.CustomerDetailWindow.BindData();
        }

        private void SetResultControls(BetAnalyzer betAnalyzer)
        {
            IEnumerable<Customer> customers;            
            Decimal winRatio = Decimal.Parse(winRatioTextBox.Text);
            Decimal multipleAboveAverageStake = Decimal.Parse(multipleAboveAverageStakeTextBox.Text);
            List<CustomerSummary> customerSummaryList = new List<CustomerSummary>();

            noBetsLoadedLabel.Visibility = Visibility.Hidden;
            noCustomersFoundLabel.Visibility = Visibility.Hidden;
            resultsDataGrid.Visibility = Visibility.Hidden;

            if (betAnalyzer == null || betAnalyzer.Bets.Count == 0)
                // No data has been loaded. User must use File Loader tab.
                noBetsLoadedLabel.Visibility = Visibility.Visible;
            else
            {                                
                customers = betAnalyzer.Customers;
                    
                if ((bool)winRatioCheckBox.IsChecked)
                    customers = customers.Where(tempCustomer => tempCustomer.WonRatio.HasValue && tempCustomer.WonRatio.Value >= winRatio / 100m);
                
                if ((bool)multipleAboveAverageStakeCheckBox.IsChecked)
                    customers = customers.Where(tempCustomer => tempCustomer.Bets.Count(tempBet => !tempBet.IsSettled && tempCustomer.GetAverageStake(BetStatus.Settled).HasValue && tempBet.Stake >= tempCustomer.GetAverageStake(BetStatus.Settled).Value * multipleAboveAverageStake) > 0);

                foreach (Customer customer in customers)
                    customerSummaryList.Add(new CustomerSummary(customer, multipleAboveAverageStake));

                if (customerSummaryList.Count() == 0)
                    noCustomersFoundLabel.Visibility = Visibility.Visible;
                else
                    resultsDataGrid.Visibility = Visibility.Visible;
                
                resultsDataGrid.ItemsSource = customerSummaryList;

                string columnHeader = String.Empty;

                if ((bool)multipleAboveAverageStakeCheckBox.IsChecked == false)
                    multipleAboveAverageStake = 1m;

                if (multipleAboveAverageStake == 1m)
                    columnHeader = "Unsettled Bets Above Average Stake";
                else
                    columnHeader = String.Format("Unsettled Bets Above Average Stake ({0} times)", Math.Round(multipleAboveAverageStake));

                resultsDataGrid.Columns.Last().Header = columnHeader;
            }
        }

        #region Private Methods - CheckBox Events

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            string tag = (string)((CheckBox)sender).Tag;

            if (tag.Equals("WinRatio"))
                if (winRatioTextBox != null)
                    winRatioTextBox.IsEnabled = true;
            else
                if (multipleAboveAverageStakeTextBox != null)
                    multipleAboveAverageStakeTextBox.IsEnabled = true;            
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            string tag = (string)((CheckBox)sender).Tag;

            if (tag.Equals("WinRatio"))           
                if (winRatioTextBox != null)
                    winRatioTextBox.IsEnabled = false;            
            else
                if (multipleAboveAverageStakeTextBox != null)
                    multipleAboveAverageStakeTextBox.IsEnabled = false;
        }
        
        #endregion

       
    }
}
