﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Globalization;
using System.Collections.ObjectModel;
using IO = System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.ComponentModel;
using Wha.Bet.UI.Windows;
using Wha.Bet.UI.Settings;
using System.Configuration;
using System.IO;
using Wha.Bet.Business;
using Business = Wha.Bet.Business;

namespace Wha.Bet.UI.UserControls
{
    /// <summary>
    /// Interaction logic for DatabaseParameterUserControl.xaml
    /// </summary>
    public partial class BetAnalysisUserControl : UserControl, IUserSettings
    {
        #region Members

        private string FileHeaderSettled = "Customer,Event,Participant,Stake,Win";
        private string FileHeaderUnsettled = "Customer,Event,Participant,Stake,To Win";

        #endregion

        #region Constructors

        public BetAnalysisUserControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// Called by MainWindow
        /// </summary>
        void IUserSettings.SaveUserSettings()
        {
            //    // Compile Settings
            //    StringCollection compileSettingsStringCollection = new StringCollection();
            //    string compileSettingsStringItem;

            //    compileSettingsDataGrid.CommitEdit();

            //    // Compile Settings Grid
            //    IEnumerable<GeneralCompileSettings> compileSettingsRows = this.GeneralCompileSettingsCollection;

            //    foreach (GeneralCompileSettings compileSettings in compileSettingsRows)
            //    {
            //        compileSettingsStringItem = String.Format(CultureInfo.InvariantCulture, "{0}|{1}|{2}|{3}|{4}|{5}|{6}", compileSettings.IsSelected.ToString(), compileSettings.ProjectFolderPath, compileSettings.ProjectFileNameFilter, compileSettings.IsRecursive, compileSettings.OutputFolderPath, compileSettings.CompilerFilePath, compileSettings.CompilerArguments);
            //        compileSettingsStringCollection.Add(compileSettingsStringItem);
            //    }

            //    Properties.Settings.Default.CompilerCompileSettingsCollection = compileSettingsStringCollection;


            //    // Log Controls
            //    Properties.Settings.Default.CompilerLogFilePath = logFilePathTextBox.Text;
            //    Properties.Settings.Default.CompilerLogSuccess = logSuccessCheckBox.IsChecked ?? false;
            //    Properties.Settings.Default.CompilerLogFailure = logFailureCheckBox.IsChecked ?? false;
        }

        #endregion

        #region Private Methods - General

        private void LoadUserSettings()
        {
            //StringCollection compileSettingsStrings = Properties.Settings.Default.CompilerCompileSettingsCollection ?? new StringCollection();

            //List<GeneralCompileSettings> generalCompileSettingsList = new List<GeneralCompileSettings>();
            //GeneralCompileSettings generalCompileSettings = null;
            //const int compileSettingsPropertyCount = 7;       // Selected, ProjectFolderPath, ProjectFileNameFilter, IsRecursive, OutputFolderPath, CompilerExecutablePath, CompilerArguments
            //bool isSelected;
            //bool isRecursive;
            //string projectFolderPath;
            //string projectFileNameFilter;
            //string outputFolderPath;
            //string compilerExecutablePath;
            //string compilerArguments;

            //string[] compileSettingsStringElements;

            //// Compile Settings
            //foreach (string compileSettingsString in compileSettingsStrings)
            //{
            //    compileSettingsStringElements = compileSettingsString.Split(new char[] { '|' });

            //    if (compileSettingsStringElements.Length == compileSettingsPropertyCount)
            //    {
            //        Boolean.TryParse(compileSettingsStringElements[0], out isSelected);
            //        projectFolderPath = compileSettingsStringElements[1];
            //        projectFileNameFilter = compileSettingsStringElements[2];
            //        Boolean.TryParse(compileSettingsStringElements[3], out isRecursive);
            //        outputFolderPath = compileSettingsStringElements[4];
            //        compilerExecutablePath = compileSettingsStringElements[5];
            //        compilerArguments = compileSettingsStringElements[6];

            //        generalCompileSettings = new GeneralCompileSettings(isSelected, projectFolderPath, projectFileNameFilter, isRecursive, outputFolderPath, compilerExecutablePath, compilerArguments);

            //        generalCompileSettingsList.Add(generalCompileSettings);

            //        this.GeneralCompileSettingsCollection.Add(generalCompileSettings);

            //    }
            //}
            //compileSettingsDataGrid.ItemsSource = this.GeneralCompileSettingsCollection; //CollectionViewSource.GetDefaultView(generalCompileSettingsList);


            //// Log File Path
            //logFilePathTextBox.Text = Properties.Settings.Default.CompilerLogFilePath;
            //logSuccessCheckBox.IsChecked = Properties.Settings.Default.CompilerLogSuccess;
            //logFailureCheckBox.IsChecked = Properties.Settings.Default.CompilerLogFailure;

        }

        private bool ValidateForm()
        {
            bool isValid = true;
            decimal amountAbove = 0;
            
            string message;

            string amountAboveText = amountAboveTextBox.Text.Trim();

            if (!Decimal.TryParse(amountAboveText, out amountAbove))
            {
                message = "Amount Equal or Above must be a number.";
                MessageBox.Show(message, "Invalid Input", MessageBoxButton.OK, MessageBoxImage.Error);
                amountAboveTextBox.Focus();
                isValid = false;
            }

            return isValid;                
        }       
     

        #endregion

        #region Private Methods - Button Events

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateForm())
            {                
                BetAnalyzer betAnalyzer = Wha.Bet.UI.Properties.Settings.Default.BetAnalyzer;

                SetResultControls(betAnalyzer);
                             
                OutputVisibility = System.Windows.Visibility.Visible;                
            }
        }
        
        #endregion       


        #region Private Methods - UserControl Events

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadUserSettings();

            OutputVisibility = System.Windows.Visibility.Hidden;
        }

        #endregion

        /// <summary>
        /// Results
        /// </summary>
        private Visibility OutputVisibility
        {
            set
            {
                resultsGroupBox.Visibility = value;             
            }
        }
                        
        #region Private Methods - Button Click Events

     
       
        #endregion


        private void SetResultControls(BetAnalyzer betAnalyzer)
        {
            IEnumerable<Business.Bet> bets;
            BetStatus betStatus = BetStatus.All;
            Decimal amountAbove = Decimal.Parse(amountAboveTextBox.Text);

            noBetsLoadedLabel.Visibility = Visibility.Hidden;
            noBetsFoundLabel.Visibility = Visibility.Hidden;
            resultsDataGrid.Visibility = Visibility.Hidden;

            if (betAnalyzer == null || betAnalyzer.Bets.Count == 0)
                // No data has been loaded. User must use File Loader tab.
                noBetsLoadedLabel.Visibility = Visibility.Visible;
            else
            {
                if ((bool)settledCheckBox.IsChecked && !(bool)unsettledCheckBox.IsChecked)
                    betStatus = BetStatus.Settled;
                else if(!(bool)settledCheckBox.IsChecked && (bool)unsettledCheckBox.IsChecked)
                    betStatus = BetStatus.Unsettled;
                
                bets = betAnalyzer.GetBets(betStatus);

                if ((bool)stakeRadioButton.IsChecked)
                    bets = bets.Where(tempBet => tempBet.Stake >= amountAbove).ToList();
                else
                    bets = bets.Where(tempBet => tempBet.Win >= amountAbove).ToList();
                  
                if (bets.Count() == 0)
                    noBetsFoundLabel.Visibility = Visibility.Visible;
                else
                    resultsDataGrid.Visibility = Visibility.Visible;

                resultsDataGrid.ItemsSource = bets;
            }
        }

        //private List<Business.Bet> Bets
        //{
        //    get;
        //    set;
        //}
               

        #region Private Properties



      
        #endregion
               
    }
}
