﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;
using System.Globalization;
using System.Collections.ObjectModel;
using IO = System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.ComponentModel;
using Wha.Bet.UI.Windows;
using Wha.Bet.UI.Settings;
using System.Configuration;
using System.IO;
using Wha.Bet.Business;
using Wha.FileManager;

namespace Wha.Bet.UI.UserControls
{
    /// <summary>
    /// Interaction logic for DatabaseParameterUserControl.xaml
    /// </summary>
    public partial class DataLoaderUserControl : UserControl, IUserSettings
    {
        #region Members

        private DataLoaderSettings dataLoaderSettings = null;

        private string FileHeaderSettled = "Customer,Event,Participant,Stake,Win";
        private string FileHeaderUnsettled = "Customer,Event,Participant,Stake,To Win";

        #endregion

        #region Constructors

        public DataLoaderUserControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Internal Methods

        #endregion


        private DataLoaderSettings DataLoaderSettings
        {
            get
            {
                if (this.dataLoaderSettings == null)
                    this.dataLoaderSettings = new DataLoaderSettings();

                return this.dataLoaderSettings;
            }
        }

        void IUserSettings.SaveUserSettings()
        {
            //this.DatabaseQuerySettings.IsLocal = localRadioButton.IsChecked ?? true;
            this.DataLoaderSettings.Save();

            Wha.Bet.UI.Properties.Settings.Default.TimeStamp = DateTime.Now.ToShortTimeString();

        }



        #region Private Methods - General

        private void LoadUserSettings()
        {
            this.DataContext = this.DataLoaderSettings;

            SetLoadedDataGroupBox(Wha.Bet.UI.Properties.Settings.Default.BetAnalyzer);

            //Set

            //Wha.Bet.UI.Properties.Settings.Default.BetAnalyzer

            //StringCollection compileSettingsStrings = Properties.Settings.Default.CompilerCompileSettingsCollection ?? new StringCollection();

            //List<GeneralCompileSettings> generalCompileSettingsList = new List<GeneralCompileSettings>();
            //GeneralCompileSettings generalCompileSettings = null;
            //const int compileSettingsPropertyCount = 7;       // Selected, ProjectFolderPath, ProjectFileNameFilter, IsRecursive, OutputFolderPath, CompilerExecutablePath, CompilerArguments
            //bool isSelected;
            //bool isRecursive;
            //string projectFolderPath;
            //string projectFileNameFilter;
            //string outputFolderPath;
            //string compilerExecutablePath;
            //string compilerArguments;

            //string[] compileSettingsStringElements;

            //// Compile Settings
            //foreach (string compileSettingsString in compileSettingsStrings)
            //{
            //    compileSettingsStringElements = compileSettingsString.Split(new char[] { '|' });

            //    if (compileSettingsStringElements.Length == compileSettingsPropertyCount)
            //    {
            //        Boolean.TryParse(compileSettingsStringElements[0], out isSelected);
            //        projectFolderPath = compileSettingsStringElements[1];
            //        projectFileNameFilter = compileSettingsStringElements[2];
            //        Boolean.TryParse(compileSettingsStringElements[3], out isRecursive);
            //        outputFolderPath = compileSettingsStringElements[4];
            //        compilerExecutablePath = compileSettingsStringElements[5];
            //        compilerArguments = compileSettingsStringElements[6];

            //        generalCompileSettings = new GeneralCompileSettings(isSelected, projectFolderPath, projectFileNameFilter, isRecursive, outputFolderPath, compilerExecutablePath, compilerArguments);

            //        generalCompileSettingsList.Add(generalCompileSettings);

            //        this.GeneralCompileSettingsCollection.Add(generalCompileSettings);

            //    }
            //}
            //compileSettingsDataGrid.ItemsSource = this.GeneralCompileSettingsCollection; //CollectionViewSource.GetDefaultView(generalCompileSettingsList);


            //// Log File Path
            //logFilePathTextBox.Text = Properties.Settings.Default.CompilerLogFilePath;
            //logSuccessCheckBox.IsChecked = Properties.Settings.Default.CompilerLogSuccess;
            //logFailureCheckBox.IsChecked = Properties.Settings.Default.CompilerLogFailure;

        }

        private bool ValidateForm()
        {
            bool isValid = true;
            
            string message;

            string filePath = filePathTextBox.Text.Trim();

            if (isValid && !filePath.IsValidFilePath())
            {
                message = "Entered file path is not valid.";
                MessageBox.Show(message, "Invalid File Path", MessageBoxButton.OK, MessageBoxImage.Error);
                filePathTextBox.Focus();
                isValid = false;
            }

            if (isValid && !File.Exists(filePath))
            {
                message = "File with betting data could not be found.";
                MessageBox.Show(message, "File Not Found", MessageBoxButton.OK, MessageBoxImage.Error);
                filePathTextBox.Focus();
                isValid = false;
            }

            if (isValid && File.Exists(filePath))
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line = reader.ReadLine();
                    
                    if (settledRadioButton.IsChecked == true)
                    {
                        if (!line.Equals(FileHeaderSettled, StringComparison.InvariantCultureIgnoreCase))
                            isValid = false;                        
                    }
                    else
                    {
                        if (!line.Equals(FileHeaderUnsettled, StringComparison.InvariantCultureIgnoreCase))
                            isValid = false;
                    }
                }

                if (!isValid)
                {
                    message = String.Format("{1}{0}{0}{2}{0}{3}", Environment.NewLine, "File does not contain required header.", "Required file header is:", (bool)settledRadioButton.IsChecked ? FileHeaderSettled : FileHeaderUnsettled);
                    MessageBox.Show(message, "File Not Found", MessageBoxButton.OK, MessageBoxImage.Error);
                    filePathTextBox.Focus();                    
                }
            }
            
            return isValid;
        }
        
        #endregion

        #region Private Methods - Compilation

        #endregion

        private void SetLoadedDataGroupBox(BetAnalyzer betAnalyzer)
        {
            if (betAnalyzer == null)
                OutputVisibility = System.Windows.Visibility.Hidden;
            else
            {
                settledBetCountLabel.Content = betAnalyzer.GetBetCount(BetStatus.Settled);
                unsettledBetCountLabel.Content = betAnalyzer.GetBetCount(BetStatus.Unsettled);
                
                OutputVisibility = System.Windows.Visibility.Visible;
            }
        }

        #region Private Methods - Button Events

        private void RunButton_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateForm())
            {
                BetAnalyzer betAnalyzer = SetBetAnalyzer();
                SetLoadedDataGroupBox(betAnalyzer);                             
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            BetAnalyzer betAnalyzer = Wha.Bet.UI.Properties.Settings.Default.BetAnalyzer;

            betAnalyzer.Clear();

            settledBetCountLabel.Content = "0";
            unsettledBetCountLabel.Content = "0";
        }

        #endregion       

        private BetAnalyzer SetBetAnalyzer()
        {   
            DataTable dataTable;
            Dictionary<string, string> mappings;
            string message = null;
            
            string filePath = filePathTextBox.Text.Trim();
            bool isSettled = (bool)settledRadioButton.IsChecked;

            TextFileReader textFileReader = new TextFileReader();
            textFileReader.FilePath = filePath;

            BetAnalyzer betAnalyzer = Wha.Bet.UI.Properties.Settings.Default.BetAnalyzer;

            if (betAnalyzer == null)
                betAnalyzer = new BetAnalyzer();
                        
            if (isSettled)
                 mappings = new Dictionary<string,string>() {
                    {"Customer", "CustomerId"},                    
                    {"Event", "EventId"},
                    {"Participant", "ParticipantId"},
                    {"Stake", "Stake"},
                    {"Win", "Win"}};
            else
                mappings = new Dictionary<string,string>() {
                    {"Customer", "CustomerId"},                    
                    {"Event", "EventId"},
                    {"Participant", "ParticipantId"},
                    {"Stake", "Stake"},
                    {"To Win", "Win"}};

            textFileReader.SetFieldToColumnMappings(mappings, 0);

            try
            {
                dataTable = textFileReader.Execute();

                betAnalyzer.LoadData(dataTable, "CustomerId", "EventId", "ParticipantId", "Stake", "Win", isSettled);
            }
            catch (IOException exception)
            {
                message = exception.Message;                
            }
            catch (Exception exception)
            {
                message = exception.Message;
            }
            
            if (!String.IsNullOrEmpty(message))
                MessageBox.Show(message, "Data Load Failed", MessageBoxButton.OK, MessageBoxImage.Error);
            
            Wha.Bet.UI.Properties.Settings.Default.BetAnalyzer = betAnalyzer;

            return betAnalyzer;
        }

        #region Private Methods - UserControl Events

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            LoadUserSettings();

            OutputVisibility = System.Windows.Visibility.Hidden;
        }

        #endregion

        /// <summary>
        /// Compile Output
        /// </summary>
        private Visibility OutputVisibility
        {
            set
            {
                loadedDataGroupBox.Visibility = value;             
            }
        }
                        
        #region Private Methods - Button Click Events

        private void BrowseFileButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            //dialog.FileName = "Document"; // Default file name
            dialog.DefaultExt = ".txt"; // Default file extension
            dialog.Filter = "CSV Files (*.csv)|*.csv|Text Files (*.txt)|*.txt|All Files (*.*)|*.*"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dialog.ShowDialog();

            // Process open file dialog box results 
            if (result == true)
            {
                // Open document 
                string filePath = dialog.FileName;

                filePathTextBox.Text = filePath;
            }

        }
       
        #endregion

       

        #region Private Properties



      
        #endregion
               
    }
}
