﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.IO;

namespace System
{
	public static class StringExtensions
	{
		#region Public Methods - Left & Right padding methods
		public static string Left(this string value, int length)
		{
            if (length > value.Length)
                return value;
            else
                //throw new ArgumentOutOfRangeException("length", "Specified length is greater than length of string.");
                return value.Substring(0, length);
		}

		public static string Right(this string value, int length)
		{
            if (length > value.Length)
                return value;
            else
                //throw new ArgumentOutOfRangeException("length", "Specified length is greater than length of string.");
                return value.Substring(value.Length - length, length);
		}

		public static bool Contains(this string value, string substring, System.StringComparison stringComparison)
		{
			if (string.IsNullOrEmpty(substring))
				return true;
			else
				return (value.IndexOf(substring, stringComparison) >= 0);
		}

		#endregion

		#region Public Methods - ToLower, ToUpper

		public static string ToLowerFirstCharacter(this string text)
		{
			return ToLowerFirstCharacter(text, CultureInfo.CurrentCulture);
		}

		public static string ToLowerFirstCharacter(this string text, CultureInfo cultureInfo)
		{
			if (String.IsNullOrEmpty(text))
				return text;
			else
				return char.ToLower(text[0], cultureInfo) + text.Substring(1);
		}

		public static string ToUpperFirstCharacter(this string text)
		{
			return ToUpperFirstCharacter(text, CultureInfo.CurrentCulture);
		}

		public static string ToUpperFirstCharacter(this string text, CultureInfo cultureInfo)
		{
			if (String.IsNullOrEmpty(text))
				return text;
			else
				return char.ToUpper(text[0], cultureInfo) + text.Substring(1);
		}

		#endregion

		#region Public Methods - Case Change

		/// <summary>
		/// Converts a string like "Local12DevelopmentNow" to "LOCAL_12_DEVELOPMENT_NOW"
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string ToBlockCase(this string text)
		{
			return ToBlockCase(text, CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Converts a string like "Local12DevelopmentNow" to "LOCAL_12_DEVELOPMENT_NOW"
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string ToBlockCase(this string text, CultureInfo cultureInfo)
		{
			// Add underscores before each capital letter (exception first letter)
			string returnValue = Regex.Replace(text, @"(\B([A-Z]|[1-9]+))", @"_$&");

			return returnValue.ToUpper(cultureInfo);
		}

		#endregion

		#region Public Methods - Insert String Based on Position of Substring

		public static string Insert(this string originalString, string insertText, string findText)
		{
			return Insert(originalString, insertText, findText, StringComparison.Ordinal);
		}

		public static string Insert(this string originalString, string insertText, string findText, StringComparison stringComparison)
		{
			return Insert(originalString, insertText, findText, stringComparison, true);
		}

		public static string Insert(this string originalString, string insertText, string findText, StringComparison stringComparison, bool insertBeforeFindText)
		{
			string newString;
			int position = originalString.IndexOf(findText, stringComparison);

			if (position >= 0)
			{
				if (insertBeforeFindText)
				{
					newString = originalString.Substring(0, position) + insertText + originalString.Substring(position);
				}
				else
				{
					// Insert after findText
					newString = originalString.Substring(0, position + findText.Length) + insertText + originalString.Substring(position + findText.Length);
				}
			}
			else
			{
				// findText not found
				return newString = originalString;
			}

			return newString;
		}

		#endregion

		#region Public Methods - Test if Number or GUID

		public static bool IsInt32(this string text)
		{
			int outputValue;
			return Int32.TryParse(text, out outputValue);
		}

		public static int ToInt32(this string text)
		{
			int outputValue;
			Int32.TryParse(text, out outputValue);
			return outputValue;
		}

		public static bool IsGuid(this string text)
		{
			bool isValid = false;
			Regex guidRegex = new Regex(@"^[{|\(]?[0-9a-fA-F]{8}[-]?([0-9a-fA-F]{4}[-]?){3}[0-9a-fA-F]{12}[\)|}]?$");

			if (text != null)
			{
				if (guidRegex.IsMatch(text))
				{
					isValid = true;
				}
			}
			return isValid;
		}

		public static Guid ToGuid(this string text)
		{
			Guid guid;

			if (text.IsGuid())
				guid = new Guid(text);
			else
				guid = Guid.Empty;

			return guid;
		}

		#endregion

		#region Public Methods - Trim

		/// <summary>
		/// Removes all leading or trailing instances of trimString.
		/// 
		/// These trim methods do not work for literal characters.
		///			string myString = @"HelloHello1H\ello";
		///			string result = myString.TrimEnd(@"H\ello");		// String is not trimmed
		/// </summary>
		/// <param name="inputString"></param>
		/// <param name="trimString"></param>
		/// <returns></returns>
		public static string Trim(this String inputString, string trimString)
		{
			string result = inputString.TrimStart(trimString).TrimEnd(trimString);

			return result;
		}

		/// <summary>
		/// Does not work if charactes of trimString must be escaped e.g. "\"
		/// </summary>
		/// <param name="inputString"></param>
		/// <param name="trimString"></param>
		/// <returns></returns>
		public static string TrimStart(this String inputString, string trimString)
		{
			Regex regex = new Regex("^(" + trimString + ")+");

			string result = regex.Replace(inputString, "");

			return result;
		}

		public static string TrimEnd(this String inputString, string trimString)
		{

			Regex regex = new Regex("(" + trimString + ")+$");

			string result = regex.Replace(inputString, "");

			return result;
		}

		#endregion

		#region Public Methods - Replace

		public static string Replace(this String inputString, int startIndex, int length, string newValue)
		{
			string returnValue = inputString;   // Returned if startIndex and length are out of range

			if (startIndex + length <= inputString.Length)
			{
				returnValue = inputString.Substring(0, startIndex) + newValue;

				if (startIndex + length < inputString.Length)
					returnValue += inputString.Substring(startIndex + length);
			}

			return returnValue;
		}

		/// <summary>
		/// String.Replace(string oldValue, string newValue) method from Microsoft is case-senstive.
		/// 
		/// The below method allows case-insensitve methods.
        /// 
        /// NOTE:
        /// It may be better to repalce this mehtod with code like the following.
        /// 
        ///     string value;
        ///     value = Regex.Replace(@"Hello there how are you? See you later.", Regex.Escape("you?"), "they?", RegexOptions.IgnoreCase);
        ///     // Returns "Hello there how are they? See you later."
        ///    
        ///     value = Regex.Replace(@"Hello there \how are you? See you later.", Regex.Escape(@"\how"), ".pow", RegexOptions.IgnoreCase);
        ///     // Returns Hello there .pow are you? See you later.
		/// </summary>
		/// <param name="inputString"></param>
		/// <param name="search"></param>
		/// <param name="replacement"></param>
		/// <param name="ignoreCase"></param>
		/// <returns></returns>
		public static string Replace(this String inputString, string search, string replacement, bool ignoreCase)
		{
			string output;
			char[] escapeBasedCharacters = new char[] { '\\', '.', '$', '^', '{', '[', '(', '|', ')', ']', '}', '*', '+', '?' };      // Must replace backslash first, otherwise replacing a replacement

			string searchPattern = search;

			foreach (char escapeBaseCharacter in escapeBasedCharacters)
				searchPattern = searchPattern.Replace(escapeBaseCharacter.ToString(), @"\" + escapeBaseCharacter);

			searchPattern = String.Format(CultureInfo.InvariantCulture, "({0})", searchPattern);   // Put search pattern brackets

			if (ignoreCase)
			{
				try
				{
					// Replace as case-insensitive
					output = Regex.Replace(inputString, searchPattern, replacement, RegexOptions.IgnoreCase);
				}
				catch (ArgumentException)
				{
					// Regex pattern is not valid
					output = inputString;
				}
			}
			else
			{
				// Case sensitive
				output = inputString.Replace(search, replacement);
			}
			return output;
		}

		#endregion

		#region Public Methods - Folder Path

		/// <summary>
		/// Examples of valid file paths:
		///		"C:\Hello\there.bat";
		///		"\\There\where\go\power.bat";
		///		"\\There\wh.ere\go\pow.er.bat";
		///		"\\There\wh.ere\pow.er.bat";
		///		"C:\pow.er.bat";
		///		
		/// Examples of invalid file paths
		///		"\\There\wh.ere\";
		///		"\\There\wh.ere";
		///		"C:\Hello";
		///		"\\Hello";
		///		"\\Hello\there";
		///		"C:\";
		///		"C:";
		///		"\\\There\where\go\power.bat";
		///		"\\There\where\\go\power.bat";
		///		
		/// Note:
		///		System.IO.Path.GetInvalidFileNameChars() returns too many invalid characters.
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static bool IsValidFilePath(this string filePath)
		{
			bool isValid;

			string invalidCharacters = @"/:*?<>""|\\";

			// Look for "C:" or "\\Share\Folder"
			// File name must have a dot with characters either side.
			Regex filePathRegex = new Regex(String.Format(CultureInfo.InvariantCulture,
											@"^(([a-zA-Z]:)|(\\\\)[^{0}]+\\[^{0}]+)(\\[^{0}]+)*(\\[^{0}\.]+\.[^{0}]+)$",
											invalidCharacters));

			isValid = filePathRegex.IsMatch(filePath);

			return isValid;

		}

        /// <summary>
        /// Examples of valid relative file paths:
        ///		"..\";
        ///		"\";
        ///		"\there";
        ///		"\there\";
        ///		"..\there\";
        ///		"there"
        ///		"there\..\..\We"
        ///		"there\.."
        ///		
        /// Examples of invalid file paths
        ///     ""
        ///		
        /// Note:
        ///		System.IO.Path.GetInvalidFileNameChars() returns too many invalid characters.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool IsValidRelativePath(this string relativePath)
        {
            bool isValid = false;

            if (!String.IsNullOrEmpty(relativePath))
            {
                string invalidCharacters = @"/:*?<>""|\\";

                // Look for zero or many instances of "..\" or "hello\" surrounded by "\" or not
                // File name must have a dot with characters either side.
                Regex filePathRegex = new Regex(String.Format(CultureInfo.InvariantCulture,
                                                @"^\\?(\.\.|[^{0}]+\\)*(\.\.|[^{0}]+)?$",
                                                invalidCharacters));

                isValid = filePathRegex.IsMatch(relativePath);
            }

            return isValid;
        }

		/// <summary>
		/// Examples of valid folder paths:
		///		"C:\Hello\there.bat";
		///     "\\There\where\go\power.bat";
		///		"\\There\wh.ere\go\pow.er.bat"
		///		"\\There\wh.ere\pow.er.bat"
		///		"C:\pow.er.bat"
		///		"\\Hello\there"
		///		"\\There\wh.ere\"
		///		"\\There\wh.ere"
		///		"C:\Hello"
		///		"C:\"
		///		"C:"
		///		
		/// Examples of invalid folder paths:
		///		"\\Hello";
		///		"\\\There\where\go\power.bat";
		///		"\\There\where\\go\power.bat";
		///		
		/// Note:
		///		System.IO.Path.GetInvalidPathChars() returns too many invalid characters.
		/// </summary>
		/// <param name="folderPath"></param>
		/// <returns></returns>
		public static bool IsValidFolderPath(this string folderPath)
		{
			bool isValid;

			string invalidCharacters = @"/:*?<>""|\\";

			// Look for "C:" or "\\Share\Folder"
			// Backslash at end is optional.
			Regex folderPathRegex = new Regex(String.Format(CultureInfo.InvariantCulture,
														@"^(([a-zA-Z]:)|(\\\\)[^{0}]+\\[^{0}]+)(\\[^{0}]+)*(\\)?$",
														invalidCharacters));

			isValid = folderPathRegex.IsMatch(folderPath);

			return isValid;
		}

        public static string GetFolderPath(this string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            return fileInfo.DirectoryName;
        }

		#endregion

        #region Public Methods - File Name and Folder Name

        public static bool IsValidFileName(this string fileName)
        {
            char[] invalidFileNameChars = Path.GetInvalidFileNameChars();
                        
            return !(fileName.ToCharArray().Intersect(invalidFileNameChars).Count() > 0);
        }

        public static bool IsValidFolderName(this string folderName)
        {
            // char[] invalidFolderNameChars = Path.GetInvalidPathChars();     // This does not appear to be accurate. Should cover following characters: \ / : * ? " < > |
            char[] invalidFolderNameChars = Path.GetInvalidFileNameChars();    // Use File Name Characters instead. Appears to be safer and includes characters such as: : * ? \ / (missing from GetInvalidPathChars)

            return !(folderName.ToCharArray().Intersect(invalidFolderNameChars).Count() > 0);
        }

        public static string GetFileName(this string filePath)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            return fileInfo.Name;
        }
        
        #endregion

    }
}
