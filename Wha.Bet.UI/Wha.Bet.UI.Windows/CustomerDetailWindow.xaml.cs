﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Wha.Bet.UI;
using System.Globalization;
using Forms = System.Windows.Forms;
using IO = System.IO;
using System.Text.RegularExpressions;
using System.IO;
using Wha.Bet.Business;
using Wha.Bet.UI.ViewModel;

namespace Wha.Bet.UI.Windows
{
    /// <summary>
    /// Interaction logic for DatabaseWindow.xaml
    /// </summary>
    public partial class CustomerDetailWindow : Window
    {
        #region Member Variables

        private CustomerDetail customerDetail = null; 

        #endregion
        
        #region Constructors

        public CustomerDetailWindow()            
        {
            InitializeComponent();
        }
        
        #endregion

        #region Private Properties

        public CustomerDetail CustomerDetail
        {
            get
            {
                return this.customerDetail;
            }
            internal set
            {
                this.customerDetail = value;
            }
        }

        #endregion

        #region Private Methods - Button Click

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {   
            this.Visibility = Visibility.Hidden;            
        }      

        #endregion
        
        #region Private Methods - Events

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //BindData();
        }

        #endregion      
        
        public void BindData()
        {
            List<BetDetail> betDetails = new List<BetDetail>();

            this.DataContext = this.CustomerDetail;

            IEnumerable<Business.Bet> bets = this.CustomerDetail.Bets;

            foreach (Business.Bet bet in bets)
                betDetails.Add(new BetDetail(bet));

            resultsDataGrid.ItemsSource = betDetails; 
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Do not close window (otherwise destroyed and not reusable.
            this.Visibility = System.Windows.Visibility.Hidden;
            e.Cancel = true;
        }
    }
}
