﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using Wha.Bet.UI.UserControls;
using System.Windows.Controls.Primitives;
using Wha.Bet.UI.Settings;

namespace Wha.Bet.UI.Windows
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Private Variables

        private Dictionary<TabItem, ToggleButton> activeToggleButtonDictionary;

        #endregion

        #region Constructors

        public MainWindow()
        {
            InitializeComponent();            
        }

        #endregion

        #region Private Methods - Window Events

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {            
            // TODO: Must load based on user settings            
            LoadUserControl(betAnalysisButton);
            SelectTabItem(betAnalysisButton);

            // HACK: First loaded UserControl does not render, so load one UserControl and then the required UserControl
            LoadUserControl(dataLoaderButton);
            SelectTabItem(dataLoaderButton);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            IEnumerable<UIElement> tempLoadedUserControls = this.LoadedUserControls;
            SaveUserSettings(tempLoadedUserControls);
            DisposeUserControls(tempLoadedUserControls);
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            // Not called            
        }

        #endregion

        private void SelectTabItem(ToggleButton toggleButton)
        {
            TabItem tabItem = tabControl.GetTabItem(toggleButton, true);

            if (tabItem != null)
                tabItem.IsSelected = true;            
        }

        #region Private Methods - User Controls

        private void LoadUserControl(ToggleButton toggleButton)
        {
            if (toggleButton != null)
            {
                // Load user control
                LoadUserControl(toggleButton.Tag as string);

                // Set all toggle buttons to IsChecked=false
                toggleButton.Parent.GetChildren<ToggleButton>(true).ForEach(tempToggleButton => tempToggleButton.IsChecked = false);

                // Ensure selected toggle button is checked
                toggleButton.IsChecked = true;
            }
        }

        private void LoadUserControl(string userControlName)
        {
            string qualifiedUserControlName = String.Format(CultureInfo.InvariantCulture, "Wha.Bet.UI.UserControls.{0}", userControlName);

            Type userControlType = Type.GetType(qualifiedUserControlName);

            LoadUserControl(userControlType);
        }

        /// <summary>
        /// Loads user control if not already a child of mainGrid.
        /// User control is then set to visible and all other controls to invisible.
        /// </summary>
        /// <param name="elementType"></param>
        private void LoadUserControl(Type userControlType)
        {         
            IEnumerable<UIElement> loadedUserControls = this.LoadedUserControls;

            UIElement userControl = loadedUserControls.FirstOrDefault(tempUIElement => tempUIElement.GetType() == userControlType);

            if (userControl == null)
            {
                // UserControl not found so add to mainGrid
                userControl = (UIElement)Activator.CreateInstance(userControlType);
                mainGrid.Children.Add(userControl);
                Grid.SetRow(userControl, 0);
                Grid.SetColumn(userControl, 1);
            }

            // Make selected control visible
            loadedUserControls.ForEach(tempUIElement => tempUIElement.Visibility = Visibility.Hidden);

            userControl.Visibility = Visibility.Visible;

            userControl.Focus();

            //userControl.InvalidateVisual();
            //this.InvalidateVisual();
        }

        private void SaveUserSettings(IEnumerable<UIElement> loadedUserControls)
        {
            IEnumerable<IUserSettings> userSettingsControls = loadedUserControls.OfType<IUserSettings>();

            userSettingsControls.ForEach(tempUserSettingsControl => tempUserSettingsControl.SaveUserSettings());

            Properties.Settings.Default.Save();
        }

        private void DisposeUserControls(IEnumerable<UIElement> loadedUserControls)
        {
            IEnumerable<IDisposable> userSettingsControls = loadedUserControls.OfType<IDisposable>();

            userSettingsControls.ForEach(tempUserSettingsControl => tempUserSettingsControl.Dispose());
        }

        #endregion

        #region Private Properties

        private IEnumerable<UIElement> LoadedUserControls
        {
            get
            {
                int rightColumnNumber = 1;

                IEnumerable<UIElement> loadedUserControls = mainGrid.Children.Cast<UIElement>().Where<UIElement>(tempUIElement => Grid.GetColumn(tempUIElement) == rightColumnNumber);

                return loadedUserControls;
            }

        }

        #endregion
        
        private Dictionary<TabItem, ToggleButton> ActiveToggleButtonDictionary
        {
            get
            {               
                if (this.activeToggleButtonDictionary == null)
                    this.activeToggleButtonDictionary = new Dictionary<TabItem, ToggleButton>();

                return this.activeToggleButtonDictionary;
            }
        }
              
        #region Private Methods - Button Click Events

        private void Button_Click(object sender, RoutedEventArgs e)
        {            
            ToggleButton toggleButton = sender as ToggleButton;

            LoadUserControl(toggleButton);

            TabItem selectedTabItem = tabControl.SelectedItem as TabItem;

            if (selectedTabItem != null)
                this.ActiveToggleButtonDictionary.Enforce(selectedTabItem, toggleButton);
            
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabItem selectedTabItem = tabControl.SelectedItem as TabItem;
            ToggleButton activeToggleButton = null;

            if (selectedTabItem != null)
            {
                if (this.ActiveToggleButtonDictionary.ContainsKey(selectedTabItem))
                {
                    activeToggleButton = this.ActiveToggleButtonDictionary[selectedTabItem];
                }
                else
                {                   
                    IEnumerable<ToggleButton> buttons = selectedTabItem.GetChildren<ToggleButton>(true);

                    Grid grid = selectedTabItem.Content as Grid;

                    if (grid != null)
                    {
                        activeToggleButton = grid.GetChildren<ToggleButton>(true).FirstOrDefault();

                        if (activeToggleButton != null)
                            this.ActiveToggleButtonDictionary.Add(selectedTabItem, activeToggleButton);
                    }
                }

                if (activeToggleButton != null)
                    LoadUserControl(activeToggleButton);
            }
        }

        private void UncheckToggleButtons(IEnumerable<ToggleButton> toggleButtons)
        {
            toggleButtons.ForEach(tempToggleButton => tempToggleButton.IsChecked = false);
        }
            
        #endregion

    }
}
