﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wha.Bet.UI.Settings
{
    [Serializable]
    public sealed class DataLoaderSettings : ApplicationSettingsBase
    {
        [UserScopedSetting()]
        [DefaultSettingValue("")]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.String)]
        public string FilePath
        {
            get { return (string)this["FilePath"]; }
            set { this["FilePath"] = value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("true")]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.String)]
        public bool IsSettled
        {
            get { return (bool)this["IsSettled"]; }
            set { this["IsSettled"] = value; }
        }
    }
}
