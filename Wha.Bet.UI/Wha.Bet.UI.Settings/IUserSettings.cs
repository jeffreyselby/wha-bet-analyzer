﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Wha.Bet.UI.Settings
{
    /// <summary>
    /// Abstract is not allowed for a control derived from UserControl (the designer has difficulty with it)
    /// </summary>
    internal interface IUserSettings
    {
        void SaveUserSettings();       
    }
}
